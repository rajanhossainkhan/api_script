<?php
/**
* This function returns all necessary table names
* 151 tables in total
* Source of table name: pdf doc :: VendorSelfCare User Guide, May 15, 2016
* Author: Rajan Hossain
* Date: 11-07-2016
*/

//Array to contain all table names
//Source :: pdf document :: VendorSelfCare User Guide , May15, 2016

function table_name_array($array_element_no){
	$table_name_array = array(
			"SubmitPayment",
			"GetProducts",
			"GetCustomerDetailsByCustomerNumber",
			"GetAvailableProducts",
			"GetDueAmountAndDate",
			"SubmitPaymentBySmartCard",
			"GetCustomerDetailsByDeviceNumber"
		 );
	return $table_name_array[$array_element_no];
}

function table_name_array_all(){
	$table_name_array_all = array(
			"SubmitPayment",
			"GetProducts",
			"GetCustomerDetailsByCustomerNumber",
			"GetAvailableProducts",
			"GetDueAmountAndDate",
			"SubmitPaymentBySmartCard",
			"GetCustomerDetailsByDeviceNumber"
		 );
	return $table_name_array_all;
}
?>
