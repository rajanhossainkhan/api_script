<?php
/**
*This script will create table in the database
*For all names in the array, there will be 3 tables
*input table, output table and history of input
*Author: Rajan Hossain
*Date: 12-07-2016
*/

/**
*Main class file include
*Method name :: as table main title include
*/
include("/var/www/api_script/class/class.config.php");
include("/var/www/api_script/includes/table_name_array.php");

//Instantiate class
$main = new Config();

//Fetch all table names
$table_name = table_name_array_all();

/**
*Loop through each table name
*generate sql query to create table
*input output and history table
*For input, external resource will 
*only input data column, so all rows
*will already need to be there 
*/

$table_query = '';
foreach ($table_name as $tn){
	//Create sql query for input table
	$table_query .= "CREATE TABLE IF NOT EXISTS ". $tn ."Input ";
	$table_query .= "(Id int PRIMARY KEY NOT NULL AUTO_INCREMENT,";
	$table_query .= "NAME varchar(255),";
	$table_query .= "DATA varchar(255),";
	$table_query .= "Mendatory varchar(10));";
	
	//Create sql query for output table
	$table_query .= "CREATE TABLE IF NOT EXISTS ". $tn ."Output ";
	$table_query .= "(Id int PRIMARY KEY NOT NULL AUTO_INCREMENT,";
	$table_query .= "NAME varchar(255),";
	$table_query .= "DATA varchar(255),";
	$table_query .= "first_load tinyint(2),";
	$table_query .= "opt_identifier int(11),";
	$table_query .= "DATE varchar(255));";


	//Create sql query for history table
	$table_query .= "CREATE TABLE IF NOT EXISTS ". $tn ."History ";
	$table_query .= "(Id int PRIMARY KEY NOT NULL AUTO_INCREMENT,";
	$table_query .= "NAME varchar(255),";
	$table_query .= "DATA varchar(255),";
	$table_query .= "DATE varchar(255),";
	$table_query .= "opt_identifier int(11),";
	$table_query .= "SourceType varchar(20));";

}

//Execute multi query
$output = $main->MultiQueryExecute($table_query);

if ($output == 1){
  $main->redirect("method_params.php");
}else {
	echo "Table Creation Failed. Please Check If All Information Was Provided Correctly.";
}

?>
