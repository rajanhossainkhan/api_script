<?php

/**
 * This script make API call for all method
 * Dynamically generates API parameter array
 * from records in the table.
 * Collect the results and send to another script to process
 */
/**
 * Recieve job serial number from crontab
 * This serial number needs to be sent to table name collector function
 * This serial number will be assigned as array element number
 * This element will be our selected method process in that sepecific job
 */
$job_serial_no = getopt('f:');
$array_element_no = $job_serial_no['f'];

include("/var/www/api_script/class/class.config.php");
require("/var/www/api_script/includes/table_name_array.php");
require("/var/www/api_script/includes/parse_api_response.php");
require ("/var/www/api_script/includes/save_history_input.php");
require("/var/www/api_script/includes/clear_input_table.php");

$main = new Config();
$url = 'http://uat.mcadigitalmedia.com/VendorSelfCare/SelfCareService.svc?singleWsdl';

//Find target table name for test
$all_table_names = table_name_array_all();
//$main->debug($all_table_names);
//exit();
//Collect current job's array element no
$table_names = table_name_array($array_element_no);
$table_name_array = array($table_names);

$params_clean = "";
$params_start = "";

foreach ($table_name_array as $tn) {

    //Build table name
    $table_name = $tn . "Input";
    $table_name_history = $tn . "History";
	$table_name_meta = $tn . "Meta";
	$table_name_output = $tn . "Output";

    //Fetch data from this table
    $output = $main->SelectALL($table_name);


    $opt_stop_flag = 0;


    if (count($output) > 0) {

        $params = "";
        $param_name = "";
        $param_value = "";
        $mendatory_flag = "";


        $params_start = array();
        foreach ($output as $data) {

            $param_name = $data->NAME;
            $param_value = $data->DATA;
            $mendatory_flag = $data->Mendatory; //Mendatory Flag 
            //If any value is found null for which mendatory value is set to yes
            if ($mendatory_flag == "yes" && ($param_value == NULL || $param_value == "")) {
                //Turn stop flag on
                $opt_stop_flag = 1;
            }

            //Build array component dynamically
            $params_start[$param_name] = $param_value;
        }

        //If a mendatory values is null, then dont trigger API call
        if ($opt_stop_flag != 1) {
            $soapclient = new SoapClient($url, array("trace" => 1, "exceptions" => 0));

            $response = $soapclient->$tn($params_start);
          
            //Build name for returned primary object
            if ($array_element_no == 4) {
                $output_primary_object_name = "GetDueAmountandDateResult";
            } else {
                $output_primary_object_name = $tn . "Result";
            }

            //Recieve value of that name
            $primary_output = $response->$output_primary_object_name;

            /**
             * Based on multiple types of response pattern
             * A function would indentify type of response
             * Based on that type, final object would be constructed
             * and sent to api parser
             * Observation on return: 
             * An array with a single object (SubmitPayment)
             * An array with multiple objects (GetAvailableProducts)
             * An array with simple elements, and objects as elements (GetCustomerDetailsByDeviceNumber)
             */
            if ($array_element_no == 2 || $array_element_no == 6) {
                //Others from 6 except get product
                $final_object = $primary_output;
            } else if ($array_element_no == 4){
                //Get amount and due date
                $final_object = $primary_output;
            } else if ($array_element_no == 1){
                //Get Products Method :: Even more complex data structure
                $final_object = $primary_output->Hardware;
            } else {
                //Submit Payment and GetAvailableProducts
                foreach ($primary_output as $val) {
                    $final_object = $val;
                }
            }

            //$main->debug($final_object);
            //exit();

            /**
            * Find time stamp
            */
            $opt_identifier_raw = time();

            /**
             * Now this result need to be dynamically parsed. 
             * Send this reponse array to a parser to process.
             */
            $parse_output = parse_api_request($final_object, $tn, $opt_identifier_raw);

            if ($parse_output > 0) {
                //Populate history table with input
                //Build query string for all insert
                //Collect date now

                date_default_timezone_set('UTC');
                $date = date('Y-m-d');
                store_history_input($params_start, $date, $opt_identifier_raw,$type = "input", $table_name_history,$table_name_meta);


                //Clear input table for new entry
                clear_input_table($table_name);

            } else if ($array_element_no == 1){
                //For get products
                date_default_timezone_set('UTC');
                $date = date('Y-m-d');
                store_history_input($params_start, $date, $opt_identifier_raw,$type = "input", $table_name_history,$table_name_meta);

                //Clear input table for new entry
                clear_input_table($table_name);
            }			
          
        }
    }
}
