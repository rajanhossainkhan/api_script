<?php
/**
* Request a wsdls file
*/
include("../class/class.config.php");
$main = new Config();
$soapclient = new SoapClient('http://uat.mcadigitalmedia.com/VendorSelfCare/SelfCareService.svc?singleWsdl', array(
                        "trace"=>1,
                        "exceptions"=>0));

$params = array(
	"VendorCode" => "IATPDStv",
	"dataSource" => "Nigeria_UAT",
	"paymentVendorCode" => "RTPP_Nigeria_IATP",
	"transactionNumber" => "",
	"customerNumber" => "32554577",
	"amount" => 9420.00,
	"invoicePeriod" => 1,
	"currency" => "NIR",
	"paymentDescription" => "TEST",
	"_ProductCollection" => "COMPLW7",
	"MethodOfPayment" => "CASH",
	"language" => "",
	"ipAddress" => "",
	"businessUnit" => "",
	"basketId" => ""
);
$main->debug($params);
echo "<br />";
$response = $soapclient->SubmitPayment($params);
var_dump($response);
?>
