<?php
/**
* Request a wsdls file
* Method Name : SubmitPaymentBySmartCard
*/
include("../class/class.config.php");
$main = new Config();
$soapclient = new SoapClient('http://uat.mcadigitalmedia.com/VendorSelfCare/SelfCareService.svc?singleWsdl', array(
                        "trace"=>1,
                        "exceptions"=>0));

$params = array(
	"VendorCode" => "IATPDStv",
	"dataSource" => "Nigeria_UAT",
	"smartQuoteRequest" => "",
	"customerNumber" => "32554577",
	"Language" => "",
	"IpAddress" => ""
);
$main->debug($params);
echo "<br />";
$response = $soapclient->GetSmartQuote($params);
$main->debug($response);
?>
