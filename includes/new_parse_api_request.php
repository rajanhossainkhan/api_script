<?php 
/**
* Recieve API response array and method name as table name
* Collect each key and value from array object
* Key will be stored in Name column in output table
* Value will be stored in Data column in output table
*/

/**
* parse api request
* params: api_output, table name
* return type: boolean
*/

require("/var/www/api_script/includes/update_output_table.php");

function first_load($api_output){
  $i = 0;
	foreach($api_output as $key=>$val){
		$i++;
	}
	return $i;
}

function parse_api_request($api_output, $table_name){
    $main = new Config();
	$fail_flag = 0;
  
	//Find if first time insert is finished
	//Substarct 1 because arrays start with 0
    $last_first_inserted =  first_load($api_output) - 1;
    
    //Build output table name
    $table_name_output = $table_name . "Output";
	$table_name_input = $table_name . "Input";
	$table_name_history = $table_name . "History";

    //Check if data in output table exist
	$check_exist = $main->SelectAll($table_name_output);
  
	//Current opt identifier
	$current_value = 0;
	
	date_default_timezone_set('UTC');
	$date = date("Y-m-d");
	
	foreach($api_output as $key => $value){
		
		//Check data type of value
	    //If array, then run another foreach
        //Save each element of that object which
		//is an element soap response array
		echo "Yeah it is here";
	    $data_type = gettype($value);
   		echo $data_type;
		exit();     

		if (count($check_exist) > 0){
      
				if (isset($check_exist{$last_first_inserted}->first_load)  && $check_exist{$last_first_inserted}->first_load == 1){				
					
					
					//If data exists then update
					foreach($check_exist as $val){
  
							$current_value = $check_exist[$last_first_inserted]->opt_identifier;
							$new_value = $current_value + 1;

             
						  	//Update process number
							//Increase number by 1 each time
							$update_result = update_output_table($val->NAME, $val->DATA,$new_value,$table_name_output, $table_name_history);
							if ($update_result == 0){
								$fail_flag = 1;
							}
					}
	
				}else {
					//Insert data :: if first time
					$output_array = array(
						"NAME" => $key,
						"DATA" => $value,
						"DATE" => $date,
						"first_load" => 1,
						"opt_identifier" => 1
					);
	 				$result = $main->insert($table_name_output, $output_array);
         
          $history_array = array(
						"NAME" => $key,
						"DATA" => $value,
						"DATE" => $date,
						"opt_identifier" => 1,
						"SourceType" => "output"
					);
          $main->insert($table_name_history, $history_array);

				  //For first time insert opt identifier is 1
			    $new_value = 1;					

					//If at least one result is wrong, then trun on fail flag
   	 			if ($result == 0){
						$fail_flag = 1;
	  			}
		
				}			
		} else {

      //Insert data :: if first time
			$output_array = array(
				"NAME" => $key,
				"DATA" => $value,
				"first_load" => 1, 
				"opt_identifier" => 1
			);
	 		$result = $main->insert($table_name_output, $output_array);

      $history_array = array(
				"NAME" => $key,
				"DATA" => $value,
				"DATE" => $date,
				"opt_identifier" => 1,
				"SourceType" => "output"
			);
     $main->insert($table_name_history, $history_array);
   
     //For first time entry, opt identifier is set to 0
     $new_value = 1;
  
			//If at least one result is wrong, then trun on fail flag
   	 	if ($result == 0){
				$fail_flag = 1;
	  	}
		}
	}

 	 //Check flag return operatin status
	if ($fail_flag == 1){
		return 0;
	}else {
		return $new_value; //Opt identifier
	}   

}
?>
