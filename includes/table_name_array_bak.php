<?php
/**
* This function returns all necessary table names
* 151 tables in total
* Source of table name: pdf doc :: VendorSelfCare User Guide, May 15, 2016
* Author: Rajan Hossain
* Date: 11-07-2016
*/

//Array to contain all table names
//Source :: pdf document :: VendorSelfCare User Guide , May15, 2016

function table_name_array($array_element_no){
	$table_name_array = array(
			"SubmitPayment",
			"GetProducts",
			"GetCustomerDetailsByCustomerNumber",
			"GetAvailableProducts",
			"InsertTransactionDetails",
			"UpdateTransactionDetails",
			"GetDefaultProduct",
			"GetPaymentList",
			"GetUpgradeproductlist",
			"GetAvailableProductsWithChannels",
			"GetChannelListFromProduct",
			"SendEmail",
			"SendSMS", 
			"GetTransactionHistory",
			"GetWalletVendorFieldInfo",
			"ActivateGOtvCustomer",
			"PaymentConfirmation",
			"Upgrade",
			"Reconnect",
			"CreateCustomerAndAccount",
			"GetByCustomerNumber",
			"GetPrimaryProducts",
			"GetAddonProducts",
			"GetPackages",
			"SendReAuthorizeToQueue",
			"CapturePackage",
			//"GetCompatibleProductsForHardware", //Not found yet
			"GetDueAmountFileDetails",
			"GetDueAmountAndDate",
			"SendSubmitPaymentToQueue",
			"GetAgentDetails",
			"AgentGetTransactionHistory",
			"SendReconnectToQueue",
			"GetCompatibleProductsbyCustomer",
			"SearchByCellPhoneNumber", 
			"GetMethodOfPayment",
			"GetAccountDetails",
			"GetDownGradeProductList",
			"DisconnectProducts",
			"GetAddOnProductsForDecSc",
			"AddProduct",
			"UpdateAccountDetails",
			"ValidateCustomerPackage",
			"Downgrade",
			"CreateCustomer",
			"GetSubscriberInFo",
			"GetPackageDetails",
			"GetCustomerDetails",
			"GetAccountType",
			"GetCustomerAddonProduct",
			"GetCustomerUpgradeProduct",

			"GetCustomerPrimaryProduct",
			"RequestPayment",
			//"SolusticePaymentConfirmation", //Not found
			"SubmitPaymentBySmartCard",
			"GetSmartQuote",
			"ReAuthorize",
			"GetCustomerDetailsByDeviceNumber"
		 );
	return $table_name_array[$array_element_no];
}

function table_name_array_all(){
	$table_name_array_all = array(
			"SubmitPayment",
			"GetProducts",
			"GetCustomerDetailsByCustomerNumber",
			"GetAvailableProducts",
			"InsertTransactionDetails",
			"UpdateTransactionDetails",
			"GetDefaultProduct",
			"GetPaymentList",
			"GetUpgradeproductlist",
			"GetAvailableProductsWithChannels",
			"GetChannelListFromProduct",
			"SendEmail",
			"SendSMS", 
			"GetTransactionHistory",
			"GetWalletVendorFieldInfo",
			"ActivateGOtvCustomer",
			"PaymentConfirmation",
			"Upgrade",
			"Reconnect",
			"CreateCustomerAndAccount",
			"GetByCustomerNumber",
			"GetPrimaryProducts",
			"GetAddonProducts",
			"GetPackages",
			"SendReAuthorizeToQueue",
			"CapturePackage",
			//"GetCompatibleProductsForHardware", //Not found yet
			"GetDueAmountFileDetails",
			"GetDueAmountAndDate",
			"SendSubmitPaymentToQueue",
			"GetAgentDetails",
			"AgentGetTransactionHistory",
			"SendReconnectToQueue",
			"GetCompatibleProductsbyCustomer",
			"SearchByCellPhoneNumber", 
			"GetMethodOfPayment",
			"GetAccountDetails",
			"GetDownGradeProductList",
			"DisconnectProducts",
			"GetAddOnProductsForDecSc",
			"AddProduct",
			"UpdateAccountDetails",
			"ValidateCustomerPackage",
			"Downgrade",
			"CreateCustomer",
			"GetSubscriberInFo",
			"GetPackageDetails",
			"GetCustomerDetails",
			"GetAccountType",
			"GetCustomerAddonProduct",
			"GetCustomerUpgradeProduct",
			//"GetCustomerDowngradeProducts", //Not found
			"GetCustomerPrimaryProduct",
			"RequestPayment",
			//"SolusticePaymentConfirmation", //Not found
			"SubmitPaymentBySmartCard",
			"GetSmartQuote",
			"ReAuthorize",
			"GetCustomerDetailsByDeviceNumber"
		 );
	return $table_name_array_all;
}

?>
