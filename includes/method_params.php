<?php
/**
* This page contains all method elements
* This element resides as an array for each element
* Author: Rajan Hossain
* Date: 16-07-2016 
*/

/**
* All methods and respective elements are in the text file named method_params.txt
* Very first word is Method Name. Such as 
* SubmitPayment-param1,param2...
* Line by line will be read and string will be seperated accordingly
*/ 

include("../class/class.config.php");
$main = new Config();

/**
* Check if first load
* if first load then redirect the page to this
* if not, then load
* This is to avoid a bug :: first time load doesnt 
* insert the database values
*/

//After relaoding after 20 seconds
if (isset($_GET["first_reload"])){
		
	//SubmitPayment Method params as table values as name
	$file_read = file("method_params.txt");
   
	$query_string = ""; 
	foreach($file_read as $info){
        
		$methods_array  = explode("-", $info);
		$method_header = $methods_array{0};  
		$name_collection_array = $methods_array{1};
	
		//Build table :: similar with table creation query
		$table_name = $method_header . "Input";	
		$query_string .= "INSERT INTO $table_name (NAME) VALUES ";
		
		$name_collections = explode(",", $name_collection_array);
		$query_string_dirty = "";

		foreach ($name_collections as $nc){
			//Build table nam:ee with method header
			$query_string_dirty .= "(\"$nc\"),";					
		}

		//Remove last comma
		$query_string_clean = rtrim($query_string_dirty, ",");
		$query_string .= $query_string_clean . ";";
       		 
	}
	$output = $main->MultiQueryExecute($query_string);
}

/**
* For 53 tables, 20 seconds is enough
* When there will be more table this refresh time
* should be more.As the tables are not available to input
* Default values in name column immeditately after they were created
* in create_table script
*/
if (!isset($_GET['first_reload'])){
    header( "refresh:20;url=method_params.php?first_reload=1" );
}
    
//After relaoding after 20 seconds
if (isset($_GET["first_reload"])){
	echo "All tables are succefully created. Tables are also prepared to recieve input from external scripts.";
}
?>
