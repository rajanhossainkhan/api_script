<?php
/**
*This script allows creating sessions
*for variables to be used to contact
*specfied database. Four different sessions will be generated
*which will be used to contact the database and create necessary tables
*/


function process_form($db_url, $db_username, $db_password, $db_name){
	$success_flag = 0;

	if ($db_url == ""){
		$err = "Please Type an Accessible Data Source Address.";
	} else if ($db_username == ""){
		$err = "Username Can Not Be Empty.";
	} else if ($db_password == ""){
		$err = "Password Can Not Be Empty";
	} else if ($db_name == ""){
		$err = "Please Type a Valid Database Name";
	} else {
		//Check if database is available at given credentials
		$con = mysqli_connect($db_url, $db_username, $db_password);
		if (!$con) {
    		$err = "Database Connection Failed!";
		}else {
			//Now check if selected database exists
			$db = mysqli_select_db($con, $db_name);
			if (!$db){
				$err = 'Database Could Not be Found!';
			} else {
				//Generate necessary sessions
				$content = $db_url . "," . $db_username . "," . $db_password . "," . $db_name;
				$fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/db_cred.txt","wb");
				fwrite($fp,$content);
				fclose($fp);
        
				$success_flag = 1;
			}
		}
	}
    
	//Build an array with error msg and opt_flag
    $output = array(
		"opt_flag" => $success_flag,
		"message" => $err
	);
    
	//Return this message and flag to front end
    return $output;
}
?>
