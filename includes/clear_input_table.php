<?php
/**
* Clean input table DATA column
* This will happen when a succesfull update will be done
* And history table will be populated accordingly
*/
function clear_input_table($table_name_input){
  	$main = new Config();
	$query = "UPDATE $table_name_input SET DATA= ''";

	$output = $main->QueryExecute($query);
	return $output;
}
?>
