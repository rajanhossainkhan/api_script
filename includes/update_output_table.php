<?php
function history_update_table($data, $new_opt_identifier, $val, $table_name_history, $opt_identifier_raw){
  date_default_timezone_set("UTC");
  $date = date("Y-m-d");
  $main = new Config();

  $history_insert_array = array(
		"DATA" => $data, 
		"NAME" => $val,
		"DATE" => $date,
		"opt_identifier" => $opt_identifier_raw,
		"SourceType" => "output"
	);
  
  //Collect existing data
  $existing_info = $main->SelectAllByCondition($table_name_history," DATA='$data' AND NAME = '$val' AND DATE = '$date' AND opt_identifier = '$opt_identifier_raw' AND SourceType = 'output'"); 
  if (count($existing_info) <=  0){
	$output = $main->insert($table_name_history, $history_insert_array);
	}else{

	}
  return 1;
}

function update_output_table($val, $data, $new_opt_identifier, $table_name_output, $table_name_history){
		$main = new Config();
		
		$info = $main->SelectAllByCondition($table_name_output, "NAME='$val'");
		$output_id = $info{0}->Id;

		date_default_timezone_set("UTC");
 		$date = date("Y-m-d");

		$update_array = array(
			"Id" => $output_id,
			"DATA" => $data,
			"opt_identifier" => $opt_identifier_raw,
			"DATE" => $date 
   	);

		$result = $main->update($table_name_output,$update_array);

    //Insert data into history table as well
    history_update_table($data, $new_opt_identifier, $val, $table_name_history,$opt_identifier_raw);
      
    
    //Insert this array into history table with same data
		if ($result == 1){
			return 1;
		} else {
			return 0;
		}
}
?>
