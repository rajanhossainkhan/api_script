<?php
/**
* Request a wsdls file
* Method name 
*/
include("../class/class.config.php");
$main = new Config();
$soapclient = new SoapClient('http://uat.mcadigitalmedia.com/VendorSelfCare/SelfCareService.svc?singleWsdl', array(
                        "trace"=>1,
                        "exceptions"=>0));

$params = array(
	"VendorCode" => "IATPDStv",
	"DataSource" => "Nigeria_UAT",
	"PaymentVendorCode" => "RTPP_Nigeria_IATP",
	"TransactionNumber" => 1,
	"SmartCardNumber" => "4115730574",
	"Amount" =>1,
	"InvoicePeriod" => 1,
	"Currency" => "NIR",
	"PaymentDescription" => "TEST",
	"ProductCollection" => "COMPLW7",
	"MethodOfPayment" => "CASH",
	"language" => "English",
	"ipAddress" => "",
	"businessUnit" => "",
	"interfaceType" => ""
);
$main->debug($params);
echo "<br />";
$response = $soapclient->SubmitPaymentBySmartCard($params);
$main->debug($response);


