<?php
/**
* Request GetCustomerDetailsByCustomerNumber method
* Test purpose for selected methods
*/

include("../class/class.config.php");
$main = new Config();
$soapclient = new SoapClient('http://uat.mcadigitalmedia.com/VendorSelfCare/SelfCareService.svc?singleWsdl', array(
                        "trace"=>1,
                        "exceptions"=>0));

$params = array(
	"dataSource" => "Nigeria_UAT",
	"customerNumber" => "32554577",
	"currencyCode" => "NIR",
	"BusinessUnit" => "",
	"VendorCode" => "IATPDStv",
	"language" => "",
	"interfaceType" => ""
);

$main->debug($params);
echo "<br />";
$response = $soapclient->GetCustomerDetailsByCustomerNumber($params);
$main->debug($response);
?>
