<?php
/**
* Request a wsdls file
* Method Name : ReAuthorize
*/
include("../class/class.config.php");
$main = new Config();
$soapclient = new SoapClient('http://uat.mcadigitalmedia.com/VendorSelfCare/SelfCareService.svc?singleWsdl', array(
                        "trace"=>1,
                        "exceptions"=>0));

$params = array(
	"dataSource" => "Nigeria_UAT",
	"smartCardNumber" => "4115730574",
	"reason" => "test",
	"VendorCode" => "IATPDStv",
	"Language" => "",
	"IpAddress" => "",
	"businessUnit" => ""
);

$main->debug($params);
echo "<br />";
$response = $soapclient->ReAuthorize($params);
$main->debug($response);
?>
