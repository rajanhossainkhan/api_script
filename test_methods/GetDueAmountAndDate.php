<?php
/**
* Request a wsdls file
* Method Name : SubmitPaymentBySmartCard
*/
include("../class/class.config.php");
$main = new Config();
$soapclient = new SoapClient('http://uat.mcadigitalmedia.com/VendorSelfCare/SelfCareService.svc?singleWsdl', array(
                        "trace"=>1,
                        "exceptions"=>0));

$params = array(
	"VendorCode" => "IATPDStv",
	"dataSource" => "Nigeria_UAT",
	"SCNumber" => "4115730574",
	"customerNumber" => "32554577",
	"Language" => "",
	"IpAddress" => "",
	"interfaceType" => ""
);
$main->debug($params);
echo "<br />";
$response = $soapclient->GetDueAmountAndDate($params);
$main->debug($response);
?>
