<?php
// This page allows users to input database source and credentials
//Author: Rajan Hossain
//Date: 11-07-2016
include("includes/database_script.php"); //Submit and Process Form
error_reporting(1);
$success_flag = '';
$message = '';
$output = array();
if (isset($_POST["submitForm"])){
	extract($_POST);
	$output = process_form($db_url, $db_username, $db_password, $db_name);
 
  $i = 1;
	//Generate error message
  foreach($output as $val){
		if ($i == 1){
			$success_flag = $val;
        } else {
			$message = $val;		
		}
    	$i++;
	}
	if ($success_flag == 1){
		header("location: includes/create_table.php");
	}	  
}
?>
<html>
<head>
	<title>Api Script</title>
	<style type="text/css">
		input {width:250px; height: 30px; background-color: lightyellow;}
	</style>
</head>
<body style='backgroung-color:whitesmoke;'>
	<h4 style="text-align:center; margin-top:10px; color: grey;">Database Credentials</h4>
	<hr>
    <?php echo '<font style="color:red;">' . $message . '</font>';?>
	<div style="width:100%">
		<form method="post">
		<font style = "">Database Access URL:</font><br />
		<input type = "text" name="db_url" /><br /><br />
		
		<!--Database user name-->
		<font style = "">Database Username:</font><br />
		<input type = "text" name="db_username" /><br /><br />

              	<!--Database password-->
		<font style = "">Database Password:</font><br />
		<input type = "text" name="db_password"/><br /><br />

		<!--Database password-->
		<font style = "">Database Name:</font><br />
		<input type = "text" name="db_name"/><br /><br />

		<input type="submit" name="submitForm" value="Submit Credentials"><br /><br />
        </form>       
		 <hr />
	</div>	
</body>
</html>
