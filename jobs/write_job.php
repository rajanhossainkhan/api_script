<?php
/**
*Write shell fill (with sh extension) for each method
*Inside the loop, after each file is written, assign
*Adequate permission; 755 permission should suffice 
*File will be executed on request from crontab
*/
//$myfile = fopen("testfile.txt", "w");


//Function with method name array
require("/var/www/api_script/includes/table_name_array.php");

//Collect table name
$table_name_array = table_name_array_all();

//Create sh file
$i = 0;
foreach($table_name_array as $tna){
	$method_name = $tna;

    $fp = fopen("run_job_$i.sh", "w");

	//Build shell file content
	$content = "#!/bin/bash \n"; //Make it readable by command
	$content .= "#Script to request API and process response \n";

	//Actual command
	$content .= "php /var/www/api_script/includes/api_request.php -f=$i";

	fwrite($fp,$content);
	fclose($fp);

	//make just created file executable
	chmod("/var/www/api_script/jobs/run_job_$i.sh", 0755);
	$i++;
}
?>
